\select@language {italian}
\select@language {italian}
\contentsline {chapter}{\numberline {1}Analisi Etnografica}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Segmentazione del Target}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}User Research}{2}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Metodi di Market Research}{2}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Contextual Inquiry}{3}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Task Analysis}{7}{subsection.1.2.3}
\contentsline {chapter}{\numberline {2}Valutazione del Sistema}{13}{chapter.2}
\contentsline {section}{\numberline {2.1}Expert Usability Review}{13}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Scelta e Adozione di Linee Guida}{13}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Prima Esplorazione del Sistema}{14}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Analisi Diretta: Sistema vs Linee Guida}{15}{subsection.2.1.3}
\contentsline {paragraph}{}{16}{section*.3}
\contentsline {subsection}{\numberline {2.1.4}Analisi Inversa: Linee Guida vs Sistema}{18}{subsection.2.1.4}
\contentsline {subsection}{\numberline {2.1.5}Rilevare e Descrivere un Numero Ragionevole di Errori}{20}{subsection.2.1.5}
\contentsline {section}{\numberline {2.2}User Testing}{21}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Definizione del Protocollo di Testing}{21}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Analisi di Dati Soggettivi e Oggettivi}{22}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Curve di Urgenza}{42}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Conclusioni ed Indicazioni}{43}{subsection.2.2.4}
\contentsline {chapter}{\numberline {3}Valutazione di altri Sistemi}{49}{chapter.3}
\contentsline {section}{\numberline {3.1}Analisi di MySolarFamily}{49}{section.3.1}
\contentsline {chapter}{\numberline {4}Studio di Fattibilit\IeC {\`a}}{51}{chapter.4}
\contentsline {section}{\numberline {4.1}Contesto d'Uso}{51}{section.4.1}
\contentsline {section}{\numberline {4.2}Personaggi}{51}{section.4.2}
\contentsline {section}{\numberline {4.3}Scenari d'Uso}{56}{section.4.3}
\contentsline {chapter}{\numberline {5}Proposta di Intervento}{63}{chapter.5}
\contentsline {section}{\numberline {5.1}Blueprint}{65}{section.5.1}
\contentsline {section}{\numberline {5.2}Prototipo dell'Interfaccia}{67}{section.5.2}
\contentsline {section}{\numberline {5.3}Proposta per il Mobile}{69}{section.5.3}
\contentsline {chapter}{\numberline {6}Valutazione dell'Intervento}{71}{chapter.6}
\contentsline {section}{\numberline {6.1}Ispezione}{71}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Analisi Diretta}{71}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Analisi Inversa}{73}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}Rilevare Errori Rimanenti}{74}{subsection.6.1.3}
\contentsline {section}{\numberline {6.2}User Testing}{75}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Protocollo di Testing}{75}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Risultati Testing}{75}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Correzione Errori e Iterazioni Successive}{93}{subsection.6.2.3}
\contentsline {chapter}{\numberline {7}Conclusioni}{95}{chapter.7}
\contentsline {section}{\numberline {7.1}Confronto UserFocus}{95}{section.7.1}
\contentsline {section}{\numberline {7.2}Confronto Gradimento SUS}{97}{section.7.2}
